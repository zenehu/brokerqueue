package main

import (
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"
)

type Queue struct {
	buffer []string
	lock   sync.Mutex
}

func (q *Queue) push(value string) {
	q.lock.Lock()
	defer q.lock.Unlock()
	q.buffer = append(q.buffer, value)
}

func (q *Queue) pop() string {
	q.lock.Lock()
	defer q.lock.Unlock()
	result := q.buffer[0]
	q.buffer = q.buffer[1:]

	return result
}

func (q *Queue) len() int {
	q.lock.Lock()
	defer q.lock.Unlock()

	return len(q.buffer)
}

type Handler struct {
	brokerQueue map[string]*Queue
	pattern     string
	lock        sync.Mutex
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		keys, isTimeout := r.URL.Query()["timeout"]

		if h.brokerQueue[h.pattern] == nil || h.brokerQueue[h.pattern].len() == 0 {
			if isTimeout {
				timeout, err := strconv.Atoi(keys[0])
				if err != nil {
					http.Error(w, "400 Bad request", http.StatusBadRequest)

					return
				}

				timeStart := time.Now()

				// Locking for FIFO type work
				h.lock.Lock()
				for {
					if time.Since(timeStart).Seconds() >= (time.Duration(timeout) * time.Second).Seconds() {
						http.Error(w, "404 Not found", http.StatusNotFound)
						h.lock.Unlock()

						return
					}

					if h.brokerQueue[h.pattern] != nil && h.brokerQueue[h.pattern].len() != 0 {
						break
					}
				}
				h.lock.Unlock()
			} else {
				http.Error(w, "404 Not found", http.StatusNotFound)

				return
			}
		}

		writtenLength, err := w.Write([]byte(h.brokerQueue[h.pattern].pop() + "\n"))
		if err != nil || writtenLength < 1 {
			http.Error(w, "500 Internal Server Error", http.StatusInternalServerError)
		}

	case http.MethodPut:
		keys, ok := r.URL.Query()["v"]

		if !ok || len(keys[0]) < 1 {
			http.Error(w, "400 Bad request", http.StatusBadRequest)

			return
		}

		if h.brokerQueue[h.pattern] == nil {
			h.brokerQueue[h.pattern] = new(Queue)
		}

		h.brokerQueue[h.pattern].push(keys[0])

	default:
		http.Error(w, "Unresolved method", http.StatusMethodNotAllowed)
	}
}

func main() {
	// Command line arguments
	if len(os.Args) != 2 {
		panic("Should be 1 argument, but you have " + strconv.Itoa(len(os.Args)-1))
	}

	handler := http.NewServeMux()

	// Handling two patterns as in the task
	handler.Handle("/color", &Handler{brokerQueue: map[string]*Queue{}, pattern: "color"})
	handler.Handle("/name", &Handler{brokerQueue: map[string]*Queue{}, pattern: "name"})

	server := http.Server{Addr: ":" + os.Args[1], Handler: handler}

	log.Fatal(server.ListenAndServe())
}
